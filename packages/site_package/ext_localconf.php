<?php
defined('TYPO3') || die();

call_user_func(function () {
    /**
     * Add default TypoScript (constants and setup)
     */
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        "@import 'EXT:site_package/Configuration/TsConfig/Page/Page.tsconfig'"
    );
});
