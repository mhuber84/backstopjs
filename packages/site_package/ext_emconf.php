<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'Backstopjs Site Package',
    'description' => 'Backstopjs auf der TYPO3 User Group München am 08.11.2021',
    'category' => 'templates',
    'author' => 'Marco',
    'author_email' => 'mail@marco-huber.de',
    'version' => '1.0.0',
    'state' => 'stable',
    'constraints' => [
        'depends' => [
            'typo3' => '11.4.0-11.5.99',
            'fluid_styled_content' => '11.4.0-11.5.99'
        ],
        'conflicts' => [
        ],
    ],
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 1
];
