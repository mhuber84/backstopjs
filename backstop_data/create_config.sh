#!/bin/bash

## Description: Create backstopjs config file
## Usage: ./backstop_data/create_config.sh [environment] [referencefrom]
## environment: ddev|staging|production The environment to test. Usually ddev or staging
## referencefrom: production|staging|ddev The environment to create reference images. Usually production
## Example: "./backstop_data/create_config.sh" Create config to compare ddev with production
## Example: "./backstop_data/create_config.sh staging" Create config to compare staging with production
## Example: "./backstop_data/create_config.sh ddev staging" Create config to compare ddev with staging

readonly ENVIRONMENT=${1}
readonly REFERENCEFROM=${2}

PATHS=("/demopages" "/demopages")
LABELS=("Home 1" "Home 2")

if [[ $REFERENCEFROM == "ddev" ]]; then
    REFERENCEBASES=("https://site1.backstopjs.ddev.site" "https://site2.backstopjs.ddev.site")
    REFERENCELABEL="ddev"
elif [[ $REFERENCEFROM == "staging" ]]; then
    REFERENCEBASES=("https://staging.site1.backstopjs.marco-huber.de" "https://staging.site2.backstopjs.marco-huber.de")
    REFERENCELABEL="staging"
else
    REFERENCEBASES=("https://production.site1.backstopjs.marco-huber.de" "https://production.site2.backstopjs.marco-huber.de")
    REFERENCELABEL="production"
fi

if [[ $ENVIRONMENT == "production" ]]; then
    TESTBASES=("https://production.site1.backstopjs.marco-huber.de" "https://production.site2.backstopjs.marco-huber.de")
    TESTLABEL="production"
elif [[ $ENVIRONMENT == "staging" ]]; then
    TESTBASES=("https://staging.site1.backstopjs.marco-huber.de" "https://staging.site2.backstopjs.marco-huber.de")
    TESTLABEL="staging"
else
    TESTBASES=("https://site1.backstopjs.ddev.site" "https://site2.backstopjs.ddev.site")
    TESTLABEL="ddev"
fi

TEMPLATE="{\"id\": \"backstop_ddev_production\",\"viewports\": [{\"label\": \"desktop\",\"width\": 1600,\"height\": 1200}],\"onBeforeScript\": \"puppet/onBefore.js\",\"onReadyScript\": \"puppet/onReady.js\",\"paths\": {\"bitmaps_reference\": \"backstop_data/bitmaps_reference\",\"bitmaps_test\": \"backstop_data/bitmaps_test\",\"engine_scripts\": \"backstop_data/engine_scripts\",\"html_report\": \"backstop_data/html_report\",\"ci_report\": \"backstop_data/ci_report\"},\"report\": [\"browser\"],\"engine\": \"puppeteer\",\"engineOptions\": {\"args\": [\"--no-sandbox\"]},\"asyncCaptureLimit\": 5,\"asyncCompareLimit\": 50,\"debug\": false,\"debugWindow\": false,\"scenarios\": [###SCENARIOS###]}"
SCENARIOTEMPLATE="{\"label\": \"###LABEL###\",\"cookiePath\": \"backstop_data/engine_scripts/cookies.json\",\"url\": \"###TESTURL###\",\"referenceUrl\": \"###REFERENCEURL###\",\"readyEvent\": \"\",\"readySelector\": \"\",\"delay\": 0,\"hideSelectors\": [],\"removeSelectors\": [],\"hoverSelector\": \"\",\"clickSelector\": \"\",\"postInteractionWait\": 0,\"selectors\": [],\"selectorExpansion\": true,\"expect\": 0,\"misMatchThreshold\" : 0.1,\"requireSameDimensions\": true}"

SCENARIOS=""
for I in 0 1; do
    echo -e "\n\n\nGet URLs from ${REFERENCEBASES[$I]}${PATHS[$I]}"
    URLS=$(curl -k ${REFERENCEBASES[$I]}${PATHS[$I]} | grep -oE 'class="menu-demopages" href="([^"#]+)"' | sed 's/class="menu-demopages" href="//g' | sed 's/"//g')
    for URL in $URLS; do
        echo -e "Create scenario for ${TESTBASES[$I]}$URL with referenceUrl ${REFERENCEBASES[$I]}${URL}"
        SCENARIO=$SCENARIOTEMPLATE
        SCENARIO=$(echo "${SCENARIO//"###LABEL###"/"${LABELS[$I]} - "${TESTLABEL}" - "${REFERENCELABEL}" - "${URL}}")
        SCENARIO=$(echo "${SCENARIO//"###TESTURL###"/${TESTBASES[$I]}${URL}}")
        SCENARIO=$(echo "${SCENARIO//"###REFERENCEURL###"/${REFERENCEBASES[$I]}${URL}}")
        SCENARIOS=$SCENARIOS","$SCENARIO
    done
done

SCENARIOS=$(echo "${SCENARIOS/,/}")

OLD="###SCENARIOS###"
CONTENT=$(echo "${TEMPLATE//${OLD}/${SCENARIOS}}")

echo -e "\n\n\nWrite backstopjs configuration file"
echo "$CONTENT" > ./backstop_data/backstop.json
